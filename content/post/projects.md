<br>

### Student Projects

Here are some of the student projects done in the Physics department at Ashoka University.

**Ankit Wenju Shrestha**

- [Thermal History of the Universe](https://drive.google.com/drive/u/1/folders/1p7GjSorCT4qChzPyOEA7b609LeFe6os7)

**Kartik Tiwari**

- [Tolman-Ehrenfest Effect in Reissner-Nordstrom Geometries: Charge, Gravity and Temperature](https://drive.google.com/drive/u/1/folders/1VW94J8wU3GGvNiVPk0pNL2gxsI72RoSi)
<br>

### The Physics Journal

<div align="center"><b>Summer 2020</b></div>

[<img src="/img/journal/2020-cover.png" alt="The Ashoka Physics Journal, Summer 2020" style="width: 200px; "/>](https://ashoka.edu.in/static/doc_uploads/file_1598960637.pdf)

### Internships and Workshops

- Arduino Workshop
