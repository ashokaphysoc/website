The Physics Society is a vibrant body of budding physicists. It conducts physics colloquiums, workshops, monitor student projects, and various outreach programs. You can find the members of the physics society for each term in this page. Current term is 2021 &#47; 22.

<details>
<summary><b style="color:blue;">2021 &#47; 22 Team</b></summary>

|Executive|Outreach|Editorial|
|---------|--------|---------|
| Ankit <br> Kartik <br> Risham | Jagat <br> Nishant <br> Tejas <br> Yerik | Anjali <br> Jenish <br> Kartikeya <br> Nitin |

</details>

<details>
<summary><b style="color:blue;">2020 &#47; 21 Team</b></summary>

|Executive|Outreach|Editorial|
|---------|--------|---------|
| Ankit <br> Kartik <br> Risham | Jagat <br> Nishant <br> Tejas <br> Yerik | Anjali <br> Jenish <br> Kartikeya <br> Nitin |

</details>
