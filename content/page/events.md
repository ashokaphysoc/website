### Past events

| Event name | Speaker | Summary | Link |
| ---------- | ------- | ------- | ---- |
| Colloquium | John Doe | This is a test summary. | [Talk link](https://en.wikipedia.org/wiki/Physics) |

### Upcoming events

| Event name | Speaker | Summary | Mode and Date |
| ---------- | ------- | ------- | ---- |
| Colloquium | John Doe | This is a test summary. | [Talk link](https://en.wikipedia.org/wiki/Physics) <br> Sept. 4, 2021, 3:00 pm <br> Meet link: [meet.google.com](meet.google.com) |
