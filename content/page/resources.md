We've compiled links to some useful resources for physics and fields related to it. You can browse them here:


- [Getting started with Python, Anaconda Distribution](https://docs.anaconda.com/anaconda/install/index.html)
- [Classical Mechanics, Lectures by Walter Lewin](https://www.youtube.com/watch?v=wWnfJ0-xXRE&list=PLyQSN7X0ro203puVhQsmCj9qhlFQ-As8e&index=1)

### Tutorials

<details><summary><b style="color:cyan;">Getting started with Tracker software</b></summary>
Tracker is a free video analysis and modeling tool built on the Open Source Physics (OSP) Java framework. It is designed to be used in physics education.
</details>
