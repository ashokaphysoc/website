### The Astro Club

The Atronomy Club, formed in 2018, is a vibrant body of budding amateur astronomers. While a significant fraction of the Astro club membership comprises of Physics majors, there are also multiple students from diverse academic backgrounds.
